/*
  Warnings:

  - Added the required column `type` to the `Suggestion` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Suggestion" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "creator" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "suggestion" TEXT NOT NULL,
    "status" TEXT NOT NULL DEFAULT 'Pending',
    "staff_member" TEXT NOT NULL,
    "staff_notes" TEXT NOT NULL,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL
);
INSERT INTO "new_Suggestion" ("created_at", "creator", "id", "staff_member", "staff_notes", "status", "suggestion", "updated_at") SELECT "created_at", "creator", "id", "staff_member", "staff_notes", "status", "suggestion", "updated_at" FROM "Suggestion";
DROP TABLE "Suggestion";
ALTER TABLE "new_Suggestion" RENAME TO "Suggestion";
CREATE UNIQUE INDEX "Suggestion_id_key" ON "Suggestion"("id");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
