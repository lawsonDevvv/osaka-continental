-- CreateTable
CREATE TABLE "Contract" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "discord" TEXT NOT NULL,
    "onward_username" TEXT NOT NULL,
    "affiliations" TEXT NOT NULL,
    "contract_placer" TEXT NOT NULL,
    "operator" TEXT NOT NULL,
    "notes" TEXT NOT NULL,
    "completed" BOOLEAN NOT NULL DEFAULT false,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL
);
