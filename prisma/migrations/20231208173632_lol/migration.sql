-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Contract" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "discord" TEXT NOT NULL,
    "onward_username" TEXT NOT NULL,
    "affiliations" TEXT NOT NULL,
    "contract_placer" TEXT NOT NULL,
    "operator" TEXT NOT NULL,
    "notes" TEXT NOT NULL,
    "completed" BOOLEAN NOT NULL DEFAULT false,
    "hunter" TEXT NOT NULL DEFAULT 'None',
    "proof" TEXT NOT NULL DEFAULT 'None',
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL
);
INSERT INTO "new_Contract" ("affiliations", "completed", "contract_placer", "created_at", "discord", "id", "notes", "onward_username", "operator", "updated_at") SELECT "affiliations", "completed", "contract_placer", "created_at", "discord", "id", "notes", "onward_username", "operator", "updated_at" FROM "Contract";
DROP TABLE "Contract";
ALTER TABLE "new_Contract" RENAME TO "Contract";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
