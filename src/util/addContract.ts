import { prisma } from "../Bot.js";

export const addContract = async (
    discord: string,
    onward_username: string,
    affiliations: string,
    contract_placer: string,
    operator: string,
    notes: string
) => {
    try {
        const createdContract = await prisma.contract.create({
            data: {
                discord,
                onward_username,
                affiliations,
                contract_placer,
                operator,
                notes
            }
        });
        return createdContract;
    } catch (e) {
        console.error(e);
        return null;
    }
}