import { Precondition } from "@sapphire/framework";
import { CommandInteraction, GuildMember } from "discord.js";

export class UserPrecondition extends Precondition {
  public async chatInputRun(interaction: CommandInteraction) {
    let staff = false;

    if ((interaction.member as GuildMember).roles.cache.has(process.env.STAFF_ROLE as string)) {
      staff = true;
    }

    if ((interaction.member as GuildMember).roles.cache.has("1181183683852501012")) {
      staff = true;
    }
    return staff
      ? this.ok()
      : this.error({ message: "Precondition Check Failure: You do not have the required permits to run this command."});
  }
}

declare module "@sapphire/framework" {
  interface Preconditions {
    Staff: never;
  }
}