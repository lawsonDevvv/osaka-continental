import { ApplyOptions } from "@sapphire/decorators";
import { ApplicationCommandRegistry, Command, CommandOptions } from "@sapphire/framework";
import { ChatInputCommandInteraction, SlashCommandBuilder, TextChannel } from "discord.js";
import { prisma } from "../../Bot.js";
import { Suggestion } from "@prisma/client";
import { EmbedBuilder } from "@discordjs/builders";

@ApplyOptions<CommandOptions>({
    description: "Accept a suggestion.",
    preconditions: ["GuildOnly", "Staff"]
})
export default class extends Command {
    public registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(o =>
                o
                    .setName("id")
                    .setDescription("The ID of the suggestion you want to accept.")
                    .setRequired(true)
            )
            .addStringOption(o =>
                o
                    .setName("reason")
                    .setDescription("The reason for accepting the suggestion.")
                    .setRequired(true)
            )
        ;

        registry.registerChatInputCommand(builder);
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction) {
        const id = interaction.options.getString("id", true);
        const reason = interaction.options.getString("reason", true);

        const suggestion = await prisma.suggestion.findUnique({
            where: {
                id
            }
        }) as Suggestion;

        if (suggestion.status === "Accepted") {
            await interaction.reply({ content: `Suggestion with ID ${id} has already been accepted.` });
            return;
        }

        if (!suggestion) {
            await interaction.reply({ content: `Suggestion with ID ${id} does not exist.` });
            return;
        }

        const suggestionEmbed = new EmbedBuilder()
            .setTitle(`${suggestion.type} Suggestion`)
            .setAuthor({ name: "Intrepid Studios 2023", iconURL: 'https://cdn.discordapp.com/avatars/415278805683404821/287fd6db88efec8e21bcd477a9e56d1d.webp?size=128' })
            .addFields(
                {
                    name: "Status",
                    value: suggestion.status,
                    inline: true
                },
                {
                    name: "Creator",
                    value: `<@${suggestion.creator}>`,
                    inline: true
                },
                {
                    name: "Staff Notes",
                    value: `${suggestion.staff_notes}`,
                    inline: true
                }
            )
            .setDescription(suggestion.suggestion)
            .setFooter({ text: `Suggestion #${id}` })
            .setColor(0x0ff00 /* Lime Green */)
        ;

        await prisma.suggestion.update({
            where: {
                id
            },
            data: {
                status: "Accepted",
                staff_notes: reason
            }
        });

        await (this.container.client.channels.cache.get(process.env.SUGGESTION_CHANNEL as string) as TextChannel).messages.fetch({message: suggestion.message}).then(async msg => {
            await msg.edit({ embeds: [suggestionEmbed] });
        });

        await interaction.reply({ content: `Suggestion with ID ${id} has been accepted.` })
    }
}