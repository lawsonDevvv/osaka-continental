import { ApplyOptions } from "@sapphire/decorators";
import { ApplicationCommandRegistry, Command, CommandOptions } from "@sapphire/framework";
import { ChatInputCommandInteraction, TextChannel } from "discord.js";
import { EmbedBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { prisma } from "../../Bot.js";

@ApplyOptions<CommandOptions>({
    description: "Mark a contract as completed",
    preconditions: ["GuildOnly", "Staff"]
})
export default class extends Command {
    public registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(o =>
                o
                    .setName("id")
                    .setDescription("The ID of the contract you want to mark as completed.")
                    .setRequired(true)
            )
            .addUserOption(o =>
                o
                    .setName("hunter")
                    .setDescription("The person who completed the contract.")
                    .setRequired(true)
            )
            .addStringOption(o =>
                o
                    .setName("proof")
                    .setDescription("Proof of completion.")
                    .setRequired(true)
            )
        ;

        registry.registerChatInputCommand(builder);
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction) {
        const id = interaction.options.getString("id", true);
        const hunter = interaction.options.getUser("hunter", true);
        const proof = interaction.options.getString("proof", true);

        const contract = await prisma.contract.findUnique({
            where: {
                id: parseInt(id)
            }
        });

        if (!contract) {
            await interaction.reply({ content: `Contract with ID ${id} does not exist.` });
            return;
        }

        await prisma.contract.update({
            where: {
                id: parseInt(id)
            },
            data: {
                completed: true,
                hunter: hunter.tag,
                proof: proof
            }
        });

        const embed = new EmbedBuilder()
        .setTitle("Contract Completed")
        .setDescription(`Contract with ID ${id} has been marked as completed.`)
        .addFields(
            {
                name: "Contract Placer",
                value: `${contract.contract_placer}`,
                inline: true
            },
            {
                name: "Hunter",
                value: `<@${hunter.id}>`,
                inline: true
            },
            {
                name: "Proof",
                value: proof,
                inline: true
            }
        )
        await interaction.reply({ content: `Contract with ID ${id} has been marked as completed.` });
        await (interaction.guild?.channels.cache.get(process.env.CONTRACT_CHANNEL as string) as TextChannel)?.send({ embeds: [embed] });

        return;
    }
}