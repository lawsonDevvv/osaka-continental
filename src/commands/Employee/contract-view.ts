import { EmbedBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import { ApplicationCommandRegistry, Command, CommandOptions, RegisterBehavior } from "@sapphire/framework";
import { ChatInputCommandInteraction } from "discord.js";
import { prisma } from "../../Bot.js";

@ApplyOptions<CommandOptions>({
    cooldownDelay: 5000,
    description: "View details about a contract.",
})
export default class extends Command {
    public registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(o =>
                o
                    .setName("id")
                    .setDescription("The ID of the contract you want to view.")
                    .setRequired(true)
            )
        ;

        registry.registerChatInputCommand(builder, {
            behaviorWhenNotIdentical: RegisterBehavior.Overwrite
        });
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction) {
        const id = interaction.options.getString("id", true);

        const contract = await prisma.contract.findUnique({
            where: {
                id: parseInt(id)
            }
        });

        if (!contract) {
            const embed = new EmbedBuilder()
                .setDescription(`Contract with ID ${id} does not exist.`)
            ;
            interaction.reply({ embeds: [embed] });
            return;
        }

        const embed = new EmbedBuilder()
            .setTitle("Contract")
            .addFields(
                {
                    name: "ID",
                    value: contract.id.toString(),
                    inline: true
                },
                {
                    name: "Discord",
                    value: contract.discord,
                    inline: true
                },
                {
                    name: "Onward Username",
                    value: contract.onward_username,
                    inline: true
                },
                {
                    name: "Affiliations",
                    value: contract.affiliations,
                    inline: true
                },
                {
                    name: "Contract Placer",
                    value: contract.contract_placer,
                    inline: true
                },
                {
                    name: "Notes",
                    value: contract.notes,
                    inline: true
                }
            )
        ;
        interaction.reply({ embeds: [embed] });
    }
}