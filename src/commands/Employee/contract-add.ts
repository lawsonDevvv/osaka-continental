import { EmbedBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import { CommandOptions, Command, ApplicationCommandRegistry, RegisterBehavior } from "@sapphire/framework";
import { ChatInputCommandInteraction, SlashCommandBuilder, TextChannel } from "discord.js";
import { addContract } from "../../util/addContract.js";
import { Contract } from "@prisma/client";

@ApplyOptions<CommandOptions>({
    description: "Post a contract on someone's head.",
    preconditions: ["Staff", "GuildOnly"],
    cooldownDelay: 10000,
})
export default class extends Command {
    public registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(o => 
                o
                    .setName("discord")
                    .setDescription("The discord of the person you want to create a contract on.")
                    .setRequired(true)
            )
            .addStringOption(o =>
                o
                    .setName("onward_username")
                    .setDescription("The onward username of the person you want to create a contract on.")
                    .setRequired(true)
            )
            .addStringOption(o =>
                o
                    .setName("affiliations")
                    .setDescription("Affiliations such as milsim, group, etc.")
                    .setRequired(true)
            )
            .addUserOption(o =>
                o
                    .setName("contract_placer")
                    .setDescription("Contract placer's Discord (Please @ them).")    
                    .setRequired(true)
            )
            .addStringOption(o => 
                o
                    .setName("notes")
                    .setDescription("Any notes you want to add to the contract.")
                    .setRequired(true)    
            )

        registry.registerChatInputCommand(builder, {
            behaviorWhenNotIdentical: RegisterBehavior.Overwrite,
        });
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction) {
        const discord = interaction.options.getString("discord", true);
        const onward_username = interaction.options.getString("onward_username", true);
        const affiliations = interaction.options.getString("affiliations", true);
        const contract_placer = interaction.options.getUser("contract_placer", true);
        const notes = interaction.options.getString("notes", true);
        
        try {
            const createdContract = await addContract(
                discord,
                onward_username,
                affiliations,
                contract_placer.tag,
                interaction.user.tag,
                notes
            ) as Contract;

            const embed = new EmbedBuilder()
                .setTitle("Contract")
                .setDescription(`Contract created by ${contract_placer} on ${discord}`)
                .addFields([
                    {
                        name: "Onward Username",
                        value: onward_username,
                        inline: true
                    },
                    {
                        name: "Affiliations",
                        value: affiliations,
                        inline: true
                    },
                    {
                        name: "Notes",
                        value: notes,
                        inline: true
                    },
                    {
                        name: "Contract Placer",
                        value: contract_placer.tag,
                        inline: true
                    },
                    {
                        name: "Operator",
                        value: interaction.user.tag,
                        inline: true
                    }
                ])
                .setFooter({ text: `Contract ID: ${createdContract.id}` })
            ;

            await (interaction.guild?.channels.cache.get(process.env.CONTRACT_CHANNEL as string) as TextChannel).send({ embeds: [embed] });
            await interaction.reply("Contract created!");
        } catch (error) {
            interaction.reply({ content: `There was an error:\n\n${error}`, ephemeral: true });
        }
    }
}