import { ApplyOptions } from "@sapphire/decorators";
import { ApplicationCommandRegistry, Command, CommandOptions, RegisterBehavior } from "@sapphire/framework";
import { ChatInputCommandInteraction, InteractionResponse, Message, SlashCommandBuilder } from "discord.js";
import { prisma } from "../../Bot.js";

@ApplyOptions<CommandOptions>({
    description: "Deny a suggestion.",
    preconditions: ["Staff", "GuildOnly"],
    cooldownDelay: 10000,
})
export default class extends Command {
    public async registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption((option) =>
                option
                    .setName("id")
                    .setDescription("The suggestion ID.")
                    .setRequired(true)
            )
            .addStringOption((option) =>
                option
                    .setName("reason")
                    .setDescription("The reason for denying the suggestion.")
                    .setRequired(true)
            )

        registry.registerChatInputCommand(builder, { behaviorWhenNotIdentical: RegisterBehavior.Overwrite });
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction): Promise<InteractionResponse<boolean> | Message<boolean> | undefined> {
        const id = interaction.options.getString("id", true);
        const reason = interaction.options.getString("reason", true);

        

        const suggestion = await prisma.suggestion.findUnique({
            where: {
                id
            },
        });

        if (!suggestion) {
            return interaction.reply({ content: `:x: No suggestion with ID ${id}. Did you check capitalization?` });
        }

        if (suggestion?.status === "Denied") {
            await interaction.reply({ content: `:x: Already denied.` });
            return;
        }
        await interaction.reply("Uno momento.")

        await prisma.suggestion.update({
            where: {
                id,
            },
            data: {
                status: "Denied",
                staff_notes: reason,
            },
        });

        return await interaction.editReply({ content: `:check:` });
    }
}