import { ApplyOptions } from "@sapphire/decorators";
import { ApplicationCommandRegistry, Command, CommandOptions, RegisterBehavior } from "@sapphire/framework";
import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";

@ApplyOptions<CommandOptions>({
    name: "shutdown",
    description: "Shutdown the bot."
})
export default class extends Command {
    registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description);

        registry.registerChatInputCommand(builder, {
            behaviorWhenNotIdentical: RegisterBehavior.Overwrite
        });
    }

    async chatInputRun(interaction: ChatInputCommandInteraction) {
        await interaction.reply({ content: "fuck this shit im retiring" });
        process.exit();
    }
}