import { SlashCommandBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import {
  ApplicationCommandRegistry,
  Command,
  type CommandOptions,
  RegisterBehavior,
} from "@sapphire/framework";
import { ChatInputCommandInteraction, EmbedBuilder } from "discord.js";

@ApplyOptions<CommandOptions>({
    name: "ping",
    description: "Test latency (likely to see if Discord is partially/fully fucked).",
})
export default class extends Command {
    registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description);

        registry.registerChatInputCommand(builder, {
          behaviorWhenNotIdentical: RegisterBehavior.Overwrite,
        });
    }

  chatInputRun(interaction: ChatInputCommandInteraction) {
      const embed = new EmbedBuilder()
          .setDescription(`Latency: ${this.container.client.ws.ping}ms!`)
          .setColor("Random");
      interaction.reply({ embeds: [embed] });
  }
}