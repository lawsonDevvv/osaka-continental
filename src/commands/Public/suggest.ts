import { ApplyOptions } from "@sapphire/decorators";
import { ApplicationCommandRegistry, Command, CommandOptions } from "@sapphire/framework";
import { ChatInputCommandInteraction, EmbedBuilder, SlashCommandBuilder, TextChannel } from "discord.js";
import { prisma } from "../../Bot.js";
import ShortUniqueId from "short-unique-id";

@ApplyOptions<CommandOptions>({
    description: "Make a suggestion for the bot/server."
})
export default class extends Command {
    public registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(o =>
                o
                    .setName("type")
                    .setDescription("Type of suggestion.")
                    .setRequired(true)
                    .addChoices({name: "Bot", value: "bot"}, {name: "Server", value: "server"})
            )
            .addStringOption(o =>
                o
                    .setName("suggestion")
                    .setDescription("Your suggestion.")
                    .setRequired(true)
            )
        ;

        registry.registerChatInputCommand(builder);
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction) {
        const type = interaction.options.getString("type", true);
        const suggestion = interaction.options.getString("suggestion", true);

        await interaction.reply("Uno momento.")

        const id = new ShortUniqueId({ length: 6 }).randomUUID()
        const createdSuggestion = await prisma.suggestion.create({
            data: {
                id,
                creator: interaction.user.id,
                suggestion,
                type,
            }
        });

        const suggestionEmbed = new EmbedBuilder()
            .setTitle(`${type} Suggestion`)
            .setAuthor({ name: "Intrepid Studios 2023", iconURL: 'https://cdn.discordapp.com/avatars/415278805683404821/287fd6db88efec8e21bcd477a9e56d1d.webp?size=128' })
            .addFields(
                {
                    name: "Status",
                    value: createdSuggestion.id,
                    inline: true
                },
                {
                    name: "Creator",
                    value: `<@${createdSuggestion.creator}>`,
                    inline: true
                },
                {
                    name: "Staff Notes",
                    value: "None",
                    inline: true
                }
            )
            .setDescription(suggestion)
            .setFooter({ text: `Suggestion #${id}` })
        ;
        // await (this.container.client.channels.cache.get(process.env.SUGGESTION_CHANNEL as string) as TextChannel).send({ embeds: [suggestionEmbed] }).then(async msg => {
        //     await msg.react("✅");
        //     await msg.react("❌");

        //     suggestionMessageID = msg.id;

        //     msg.startThread({
        //         name: `Suggestion ${id} Discussion`,
        //         reason: `Suggestion ${id} Discussion`
        //     }).then(async thread => {
        //         thread.send({
        //             content: `Be civil, don't get muted, don't get banned.`,
        //             allowedMentions: {
        //                 parse: ["users"]
        //             }
        //         });
        //     });
        // });        
        const embed = new EmbedBuilder()
            .setTitle("Suggestion")
            .setDescription(`Suggestion #${id} created.`)
        ;
        
        await (this.container.client.channels.cache.get(process.env.SUGGESTION_CHANNEL as string) as TextChannel).send({ embeds: [suggestionEmbed] })

        await interaction.editReply({ content: "", embeds: [embed] })
        
    }
}