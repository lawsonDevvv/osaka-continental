import { ApplyOptions } from "@sapphire/decorators";
import { CommandOptions, Command, ApplicationCommandRegistry, RegisterBehavior } from "@sapphire/framework";
import { ChatInputCommandInteraction } from "discord.js";
import { EmbedBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { editsnipes, snipes } from "../../Bot.js";

@ApplyOptions<CommandOptions>({
    description: "Snipe a deleted message.",
    cooldownDelay: 5000,
})
export default class extends Command {
    public registerApplicationCommands(registry: ApplicationCommandRegistry) {
        const builder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(option => 
                option.setName("type").setDescription("Choose whether to snipe a message or an edit.")
                .addChoices(
                    {
                        name: "Message",
                        value: "msg"
                    },
                    {
                        name: "Edit",
                        value: "edit"
                    }
                )
            )

        registry.registerChatInputCommand(builder, {behaviorWhenNotIdentical: RegisterBehavior.Overwrite});
    }

    public async chatInputRun(interaction: ChatInputCommandInteraction) {
        const type = interaction.options.getString("type", true);

        if (type === "msg") {
            const msg = snipes.get(interaction.channelId);

            if (!msg) return interaction.reply({content: "There's nothing to snipe.", ephemeral: false});

            const embed = new EmbedBuilder()
                .setTitle("Sniped Message")
                .setDescription(`> ${msg.content}\n- ${msg.author.tag}`)
                .setFooter({text: "BEAMED BY MERCH, FUCK ALL THE OPPS"})

            return interaction.reply({embeds: [embed], ephemeral: false});
        } else if (type === "edit") {
            const msg = editsnipes.get(interaction.channelId);

            if (!msg) return interaction.reply({content: "There's nothing to snipe.", ephemeral: false});

            const embed = new EmbedBuilder()
                .setTitle("Sniped Message-Edit")
                .addFields({
                    name: "Old",
                    value: msg[0].content,
                    inline: true
                },
                {
                    name: "New",
                    value: msg[1].content,
                    inline: true
                },
                {
                    name: "From",
                    value: msg[0].author.tag,
                    inline: true
                })
                .setFooter({text: "BEAMED BY MERCH, FUCK ALL THE OPPS"});

            return interaction.reply({embeds: [embed], ephemeral: false});
        } else {
            return null;
        }
    }
}